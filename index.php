<?php
/*
  Template Name: Home
*/
get_header();
?>

<div class="home-tiles gallery" id="home-tiles">
				<div class="row home1">
					<div class="image" id="home-logo">
						<a href="http://yourdaydesign.fi/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_circle_big.png" alt="" width="680" height="680" />
            </a>
          </div>
					<div class="image" id="image1">
						<a href="<?php echo get_page_link(get_page_by_title(Gallery)); ?>">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/image11.jpg" alt="" height="614" width="1284" />
            </a>
          </div>
				</div>
				<div class="row home2">
					<div class="image" id="image2">
						<a href="<?php echo get_page_link(get_page_by_title(Services)); ?>">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img_services.jpg" alt="" width="1316" height="614"/>
            </a>
          </div>
					<div class="image" id="image3">
						<a href="<?php echo get_page_link(get_page_by_title(About)); ?>">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/image_about.jpg" alt="" width="854" height="615" /></a>
          </div>
				</div>
				<div class="row home3">
					<div class="image" id="image4">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flowers.png" alt="" width="2198" height="307" />
          </div>
				</div>
				<div class="row home4">
					<div class="image" id="image5">
            <a href="<?php echo get_page_link(get_page_by_title(Contest)); ?>">
						<img src="http://annebook.com/wp-content/uploads/2015/12/Martha_Quote.png" alt="" width="1317" height="614" /></a>
          </div>
					<div class="image" id="image6">
						  <a href="<?php echo get_page_link(get_page_by_title(Contact)); ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img_contact.jpg" alt="" width="854" height="614" /></a>
          </div>
				</div>
			</div>
			<script>
				homeTiles("home-tiles", .01181818182);

        $("header").css("display", "none");
			</script>

<?php get_footer(); ?>
