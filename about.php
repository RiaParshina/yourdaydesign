<?php get_header(); ?>
<?php
  /*
    Template Name: About
  */
?>

<div class="bg"></div>
<div class="row">
				<div class="page-features">
					<div class="feature-image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/julia2.jpg" alt="" />
					</div>
					<p>"It all started after one night at the computer, when I got almost 30 people involved in creating a new wedding agency, with a new idea and concept. That feeling when people are backed into something completely unknown and go on with you is incredible!"</p>
				</div>
				<div class="page-content">
					<div class="columns">
					<p><span style="font-family:Aquarelle; font-size:30px;">I</span> am a 28-year-old executive assistant, graduated from Haaga-Helia University a few years ago. I have been 100% sure since graduating from high school that there is something bigger and more exciting for me, than the dull routine of office work. Since childhood I have been interested in music, dance, and I have been really creative, but I did not really know, what it could be used for.</p>

          <p>As soon as I was able to start the wedding planning for the first time, I realized that this is exactly what was missing from my life. Helping and being backed up by the most important things that all of us need in everyday life. Being supported in the most difficult moments is even more important. Weddings are a really big step for couples.</p>

          <p>The fact that I get to be a part of this moment in a pair´s life makes me really happy. All this is crowned with professional´s work, who can organize everything according to just a customer's mind. If there is a need to go to the moon for something, I will do it!</p>

          <p>Your Day Design is a professional team, which is capable of everything; this is a company, where you can find what you need for celebration. We are a big group of creative, interested young entrepreneurs who are able to sew from Your Day unforgettable!</p>

          <p>Your Day Design can also organize different types of other events, except the wedding. We are able to organize birthday parties, children's parties, bachelor party, proposals, corporate parties and Christmas parties.</p>

          <p>You will get really unique, friendly and heartfelt customer service.</p>

          <p>Welcome to bring Happiness from Your Day!</p>
					</div>
				</div>
			</div>
<?php get_footer(); ?>
