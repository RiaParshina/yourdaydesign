<?php
  /*
    Template Name: Contest FI
  */
?>
<?php get_header(); ?>

  <div class="container">
    <div class="row bg_contest">
      <div class="banner">
        <h2><span>VOITA</span><br>kokonaisvaltainen juhlasuunnittelu&shy;paketti!</h2><br>
        <h1>-30%</h1>
      </div>
    </div>
    <div class="row bg_contest2">
      <h2>Pakettiin kuuluu:</h2>
      <div style="width:40%; padding:10px 10px 0 0; float:left;">
          <ul class="dotted_ul">
            <li>Budjetti, suunnittelu ja päivän ohjelma</li>
            <li>Teeman valinta</li>
            <li>Vihki- ja juhlapaikan löytäminen ja varaaminen</li>
            <li>Boudour- ja häämekot</li>
            <li>Kutsu- ja kiitoskortit, menu, istumajärjestys</li>
            <li>Koreografi</li>
            <li>Valokuvaaja ja videokuvaaja</li>
          </ul>
      </div>
      <div style="width:40%; padding:10px 10px 0 0; float:right;">
          <ul class="dotted_ul">
            <li>Somistus ja kukat</li>
            <li>Meikki, kampaus ja manikyyri</li>
            <li>Juhlakakku ja catering</li>
            <li>Artistit ja Dj</li>
            <li>Häämatkat</li>
            <li>Juhlakoor&shy;dinaattori, 24/7 konsultointi, tuki ja turva</li>
          </ul>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div class="contact-form">
       <?php
       $content = apply_filters('the_content', $post->post_content);
       echo $content;
      ?>
    </div>
  </div>

  <style>
    .banner h2 {
      hyphens: manual;
    }
    .banner h2 span {
      letter-spacing: 1px;
    }
    @media only screen and (max-width: 570px) {
      .banner {
        height: 340px;
      }
      .dotted_ul li {
        hyphens: manual;
      }
    }
  </style>

<?php get_footer(); ?>
