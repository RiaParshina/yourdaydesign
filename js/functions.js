// JavaScript Document

$(document).ready(function() {

	$(function() {
	   $( "#date" ).datepicker();
	});

	$(".mobile-social").html($(".social").html());

	$( ".mobile-nav" ).click(function() {
	  $( ".nav-elements" ).toggle(400);
	});
});

$(window).load(function(){



});

$(window).resize(function() {
	displayNav();


});

$(window).scroll(function () {


});

function homeTiles(galleryID, margin) {
	var gap = .01181818182;
	var gap = margin;

	$("#"+galleryID).each(function() {
		$(".row").each(function() {
			var imageCount = 0;
			var images = new Array();
			$(this).find('.image').each(function() {
				imageCount++;
				var imageID = $(this).attr("id");
				var imageWidth = Number($(this).find("img").attr("width"));
				var imageHeight = Number($(this).find("img").attr("height"));
				var sizes = {
					width: imageWidth,
					height: imageHeight
				};
				images[imageID] = sizes;

			});

			// $('#home-logo').css('width', '31%');

			var gaps = imageCount - 1;
			var gapPercent = gaps * gap;

			var fullWidth = 0;
			for (var id in images) {
				//Set an arbitrary height. We're just evaluating the difference in width.
				var arbitraryHeight = 100;

				var relativeWidth = (images[id]["width"] * arbitraryHeight)/images[id]["height"];
				images[id]["relativeWidth"] = relativeWidth;
				fullWidth += relativeWidth;
			}

			for (var id in images) {
				var percentOfImages = images[id]["relativeWidth"]/fullWidth;
				var percentOfTotal = 100 * (percentOfImages * (1 - gapPercent));

				$("#"+id).css({
					width : percentOfTotal+"%"
				});
			}


		});
	});
}
function displayNav() {
	if ($(window).width() > 653) {
		$( ".nav-elements" ).css("display", "block");
	} else {
		$( ".nav-elements" ).css("display", "none");
	}
}
