<?php
  /*
    Template Name: Contest
  */
?>
<?php get_header(); ?>

  <div class="container">
    <div class="row bg_contest">
      <div class="banner">
        <h2><span>Win</span><br>a full package of wedding services!</h2><br>
        <h1>-30%</h1>
      </div>
    </div>
    <div class="row bg_contest2">
      <h2>The package includes:</h2>
      <div style="width:50%; padding:10px 10px 0 0; float:left;">
          <ul class="dotted_ul">
            <li>Budget, wedding day design, wedding day plan</li>
            <li>Wedding style and theme</li>
            <li>Wedding venue</li>
            <li>Boudoir and wedding dresses</li>
            <li>Invitations, Save the date- , menu- cards, seating charts</li>
            <li>Wedding dance choreography</li>
            <li>Photography, videography</li>
          </ul>
      </div>
      <div style="width:40%; padding:10px 10px 0 0; float:right;">
          <ul class="dotted_ul">
            <li>Wedding decor, floristics, wedding bou&shy;quet</li>
            <li>MUAH, manicure, cosmeto&shy;logist</li>
            <li>Catering and wedding cake</li>
            <li>Artists, musical accompa&shy;niment</li>
            <li>Honeymoon</li>
            <li>Wedding coordinator</li>
          </ul>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div class="contact-form">
       <?php
       $content = apply_filters('the_content', $post->post_content);
       echo $content;
      ?>
    </div>
  </div>


<?php get_footer(); ?>

<style>
  @media only screen and (max-width: 414px) {
    .dotted_ul li {
      hyphens: manual;
    }
  }
</style>
