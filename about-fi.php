<?php get_header(); ?>
<?php
  /*
    Template Name: About FI
  */
?>

<div class="bg"></div>
<div class="row">
				<div class="page-features">
					<div class="feature-image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/julia2.jpg" alt="" />
					</div>
					<p>"Kaikki lähti siitä, kun yhden yön aikana koneen ääressä sain melkein 30 ihmistä mukaan luomaan uutta hää toimistoa, uudella idealla ja konseptilla. Se tunne, kun ihmiset ovat tukena ja lähtevät mukaasi tuntemattomaan on uskomaton!"</p>
				</div>
				<div class="page-content">
					<div class="columns">
					<p><span style="font-family:Aquarelle; font-size:30px;">O</span>len 28-vuotias johdon assistentti, joka valmistui Haaga-Heliasta muutama vuosi sitten. Olin ollut lukiosta lähtien varma, että minua odottaa jotain isompaa ja kiinnostavampaa, kun tylsä rutiininomainen toimistotyö. Lapsuudesta lähtien olen harrastanut musiikkia, tansseista ja olen ollut todella luova, mutta en oikein tiennyt, mihin sitä luovuutta voisi käyttää.</p>

          <p>Heti kun pääsin ensimmäisen kerran käsiksi hääsuunnitteluun, ymmärsin, että tämähän on ollut juuri se, mikä puuttuu elämästäni. Auttaminen ja oleminen tukena ovat tärkeimmistä asioista, jotka jokainen meistä tarvitsee arkielämässäkin. Oleminen tukena vaikeimpina hetkinä on vielä tärkeämpää. Häät ovat todella iso askel pariskunnille.</p>

          <p>Se, että pääsen olemaan osana tätä hetkeä elämästänsä tekevät minusta onnellisen. Kruunauksena on se, että ammattilaisten avulla saan järjestettyä ihan mitä asiakkaan mieleen tulee. Jos sitä varten minun pitäisi käydä kuun päällä, niin minä teen sen!</p>

          <p>Your Day Design- on ammattilaistiimi, joka pystyy kaikkeen; yritykseni, josta löytyy kaikki, mitä juhlaan tarvitset. Olemme iso ryhmä luovia, kiinnostuneita nuoria yrittäjiä, jotka pystyvät ompelemaan päivästänne unohtumattoman!</p>

          <p>Your Day Designilta saa tilattua muitakin juhlia, kun häitä. Me pystymme järjestämään syntymäpäiviä, lastenjuhlia, polttareita, kosintoja, yritysjuhlia ja pikkujouluja.</p>

          <p>Meiltä saa todella yksilöllistä, ystävällistä ja sydämellistä asiakaspalvelua.</p>

          <p>Tervetuloa ompelemaan päivästänne juhlaa!</p>

					</div>
				</div>
			</div>
<?php get_footer(); ?>
