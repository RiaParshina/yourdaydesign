<!DOCTYPE html>
<html>
	<head>
		<base href="" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

		<title>Your Day Design</title>

		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="  crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"   integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

		<script type="text/javascript" src="js/functions.js"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?> >
		<div class="container">
			<ul id="language-switcher"><?php pll_the_languages();?></ul>
			<header>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="logo">
				</div>
				</a>
				<nav class="header-nav">
					<div class="mobile-nav"><i class="fa fa-bars"></i></div>
					<div class="nav-elements">
						<?php wp_nav_menu(array("theme_location" => "main_nav")); ?>
            <div class="mobile-social"></div>
					</div>
				</nav>
				<div class="social">
					<ul>
						<li><a href="https://www.facebook.com/yourdaydesign/" target="_blank"><i class="fa fa-facebook-official"></i></a></li>
						<li><a href="https://vk.com/your_day_design" target="_blank"><i class="fa fa-vk"></i></a></li>
						<li><a href="https://www.instagram.com/your_day_design/" target="_blank"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
				<div class="location-bar">
					<div class="location-left"><?php pll_e('Helsinki'); ?></div><div class="location-right"><?php pll_e('Finland'); ?></div>
				</div>
			</header>


			<style>
				li a {
					color: #c47d83;
				}
			</style>
