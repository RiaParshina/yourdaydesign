<!DOCTYPE html>
<html>
	<head>
		<base href="" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

		<title>Your Day Design</title>

		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="  crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"   integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="js/functions.js"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?> >
		<div class="container">
			<ul id="language-switcher"><?php pll_the_languages();?></ul>
			<header>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<div class="logo" style="width: 400px; height: 300px;"><h2>Wedding and event planning</h2></div>
				</a>
				<nav class="header-nav">
					<div class="mobile-nav"><i class="fa fa-bars"></i></div>
					<div class="nav-elements">
						<?php wp_nav_menu(array("theme_location" => "main_nav")); ?>
						<div class="mobile-social"></div>
					</div>
				</nav>

				<a href="<?php
				$current_lang = pll_current_language();
			  $contest_url = get_page_link(get_page_by_title(Contest));
			  $contest_id = url_to_postid($contest_url);
			  $target_contest = pll_get_post($contest_id, $current_lang);
			  the_permalink($target_contest);
			  ?>"><div id='banner-index'>
					<h2 id='contest-link'>Win a full package of wedding services!</h2>
					<h1 id='discount'>-30%</h1>
				</div></a>
			</header>

			<style>
      .logo h2 {
        text-align: center;
        font-family: Aquarelle;
        font-size: 1.3em;
        margin-bottom: 7px;
        color: white;
        margin-top: 68%;
        letter-spacing: 3px;
      }
			#language-switcher li a {
				color: white;
			}
			.logo {
				margin-left: -200.5px;
				margin-top: -111.5px;
        margin-bottom: 1%;
        padding-bottom: 4px;
			}
			.menu {
				border-top: 1px solid rgba(255, 255, 255, 0.5);
				border-bottom: 1px solid rgba(255, 255, 255, 0.5);
				margin-top: 340px;
			}
			.menu li {
				display: inline;
				font-size: 1.8em;
			}
			.menu li a {
				margin-left: 8%;
				margin-right: 6%;
			}
			#menu-navmenu_fi li a {
				margin-left: 6%;
				margin-right: 6%;
			}
			#banner-index {
				margin-top: 0px;
				background-color: rgba(255, 255, 255, 0.6);
				width: 50%;
				margin-left: auto;
				margin-right: auto;
				border-radius: 4px;
				text-align: center;
				padding: 1.5%;
				border: 4px solid #fff;
			}
			#banner-index:hover {
				background-color: #fff;
			}
			#banner-index h2 {
				margin-top: 0px;
				font-family: Calibri;
				font-size: 1.4em;
				color: #666;
			}
			#banner-index h1 {
				color: red;
				font-size: 2.3em;
			}

			@media only screen and (max-width: 983px) {
				.menu li a {
					margin-left: 8%;
					margin-right: 5%;
					font-size: 22px;
				}
				#menu-navmenu_fi li a {
					margin-left: 7%;
					margin-right: 5%;
					font-size: 22px;
				}
			}
			@media only screen and (max-width: 950px) {
				.menu li a {
					margin-left: 7%;
					margin-right: 5%;
				}
				#menu-navmenu_fi li a {
					margin-left: 6%;
					margin-right: 5%;
					font-size: 20px;
				}
			}
			@media only screen and (max-width: 843px) {
				.menu li a {
					font-size: 20px;
				}
				#menu-navmenu_fi li a {
					margin-left: 5%;
					margin-right: 5%;
				}
			}
			@media only screen and (max-width: 773px) {
				#menu-navmenu_fi li a {
					margin-left: 5%;
					margin-right: 4%;
				}
			}
			@media only screen and (max-width: 740px) {
				.menu li a {
					margin-left: 6%;
					margin-right: 4%;
				}
				#menu-navmenu_fi li a {
					margin-left: 4%;
					margin-right: 4%;
				}
        .logo {
          margin-left: -200px;
          margin-top: -120px;
        }
        #language-switcher {
          width: 100%;
        }
			}
			@media only screen and (max-width: 686px) {
				#menu-navmenu_fi li a {
					margin-left: 4%;
					margin-right: 3%;
				}
			}
			@media only screen and (max-width: 653px) {
				.logo {
					margin-left: -200px;
					margin-top: -120px;
				}
				#banner-index {
					width: 70%;
					margin-top: 300px;
				}
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					left: 30px;
					top: 5px;
					color: white;
				}
				.nav-elements {
          top: 350px;
          width: 100%;
        }
				.nav-elements a {
					color: #c47d83;
				}
				.nav-elements a:hover {
					color: #E6EBED;
				}
				.menu {
					margin-top: 0px;
					margin-left: 0px;
					line-height: 1.2;
				}
				.menu li a {
					display: block;
					margin-left: 0px;
				}
				.mobile-social {
					display: none;
				}
				.social {
					display: inline-block;
				}
				.nav-elements {
					padding: 7px 0;
				}
			}
			@media only screen and (max-width: 570px) {
        .menu {
          width: 100%;
          border: none;
        }
			}
			@media only screen and (max-width: 431px) {
				.logo {
					margin-left: -255px;
				}
				#banner-index {
					width: 80%;
				}
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					left: 20px;
				}
				#language-switcher {
					margin-left: 3%;
				}
			}
			@media only screen and (max-width: 414px) {
				.logo {
					margin-left: -235px;
					margin-top: -150px;
					width: 110% !important;
				}
				#banner-index {
					width: 80%;
					margin-left: 9%;
				}
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					position: relative;
					left: 780%;
					top: 100px;
				}
				#language-switcher {
					margin-left: 4%;
				}
			}
			@media only screen and (max-width: 395px) {
				#banner-index {
					margin-left: 8%;
				}
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					left: 730%;
				}
				.logo {
					margin-left: -225px;
				}
				.logo h2 {
					margin-top: 72%;
				}
			}
			@media only screen and (max-width: 375px) {
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					left: 670%;
				}
				.logo {
					margin-left: -214px;
				}
				.logo h2 {
					margin-top: 76%;
				}
				#banner-index {
					width: 90%;
					margin-left: 3%;
				}
			}
			@media only screen and (max-width: 360px) {
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					left: 645%;
				}
				.logo {
					margin-left: -205px;
				}
				.logo h2 {
					margin-top: 79%;
				}
			}
			@media only screen and (max-width: 340px) {
				.logo {
					margin-left: -195px;
				}
				.logo h2 {
					margin-top: 81%;
				}
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					left: 595%;
				}
			}
			@media only screen and (max-width: 320px) {
				.fa-navicon:before,
				.fa-reorder:before,
				.fa-bars:before {
					left: 550%;
				}
				.logo {
					margin-left: -182px;
				}
				.logo h2 {
					margin-top: 85%;
					font-size: 1.2em;
				}
				#banner-index {
					margin-left: 2%;
				}
			}
			</style>

			<?php get_footer('home'); ?>
