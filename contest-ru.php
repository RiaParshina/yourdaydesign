<?php
  /*
    Template Name: Contest RU
  */
?>
<?php get_header(); ?>

  <div class="container">
    <div class="row bg_contest">
      <div class="banner banner-ru">
        <h2><span>Выйграй</span><br>комплексный пакет сваденых услуг под ключ!</h2><br>
        <h1>-30%</h1>
      </div>
    </div>
    <div class="row bg_contest2">
      <h2>В пакет услуг входит:</h2>
      <div style="width:40%; padding:10px 10px 0 0; float:left;">
          <ul class="dotted_ul">
            <li>Составление свадебного бюджета; Выбор тематики, сценария свадьбы, разработка тайминг-плана</li>
            <li>Выбор стиля свадьбы</li>
            <li>Выбор места для проведения банкета, фуршета, места для проведения торжественной церемонии бракосочетания, венчания</li>
            <li>Будуарные и свадебные платья</li>
            <li>Изготовление приглашений, схемы рассадки гостей на торжественном ужине, карточки для гостей, меню банкета, комплименты для гостей</li>
            <li>Постановка свадебного танца</li>
            <li>Осуществление профессиональной фото и видеосъемки праздничного торжества</li>
          </ul>
      </div>
      <div style="width:40%; padding:10px 10px 0 0; float:right;">
          <ul class="dotted_ul">
            <li>Свадебный декор и флористика, свадебный букет для невесты</li>
            <li>Услуги стилистов и визажистов, мастеров маникюра, педикюра, косметолога для жениха и невесты</li>
            <li>Фуршет и предложе&shy;ние различных вариантов свадебного торта</li>
            <li>Выбор артистов, подборка музыкаль&shy;ного сопровож&shy;дения</li>
            <li>Свадебные путешествия</li>
            <li>Бесценная гарантия и тщательный контроль за меропри&shy;ятием, помощь персональ&shy;ного свадебного координа&shy;тора</li>
          </ul>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div class="contact-form">
       <?php
       $content = apply_filters('the_content', $post->post_content);
       echo $content;
      ?>
    </div>
  </div>


<?php get_footer(); ?>

<style>
  @media only screen and (max-width: 414px) {
    .banner {
      width: 300px;
    }
    .dotted_ul li {
      hyphens: manual;
    }
  }
</style>
