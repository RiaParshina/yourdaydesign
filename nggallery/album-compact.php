<?php
/**
Template Page for the album overview

Follow variables are useable :

    $album     	 : Contain information about the first album
    $albums    	 : Contain information about all albums
	$galleries   : Contain all galleries inside this album
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
?>
<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?><?php if (!empty ($galleries)) : ?>

<div class="ngg-albumoverview">

	<!-- List of galleries -->
	<?php foreach ($galleries as $gallery) : ?>

	<div class="ngg-album-compact">
		<div class="ngg-album-compactbox">
			<div class="ngg-album-link">
				<a class="Link" href="<?php
				$current_lang = pll_current_language();
			  $ngg_url = nextgen_esc_url($gallery->pagelink);
			  $en_post_id = url_to_postid($ngg_url);
				$target_id = pll_get_post($en_post_id, $current_lang);
				if ($target_id == false) {
					the_permalink($en_post_id);
				} else {
					the_permalink($target_id);
				}
				?>">
					<img class="Thumb" alt="<?php
					if ($target_id == false) {
						echo get_the_title($en_post_id);
					} else {
						echo get_the_title($target_id);
					}
					?>" src="<?php echo nextgen_esc_url($gallery->previewurl); ?>"/>
				</a>
			</div>
		</div>
        <?php if (!empty($image_gen_params)) {
            $max_width = 'style="max-width: ' . ($image_gen_params['width'] + 20) . 'px"';
        } else {
            $max_width = '';
        } ?>
        <h4>
            <a class="ngg-album-desc"
               title="<?php if ($target_id == false) {
		 						echo get_the_title($en_post_id);
		 					} else {
		 						echo get_the_title($target_id);
		 					} ?>"
               href="<?php if ($target_id == false) {
			 					the_permalink($en_post_id);
			 				} else {
			 					the_permalink($target_id);
			 				} ?>"
               <?php echo $max_width; ?>>
                <?php if ($target_id == false) {
									echo get_the_title($en_post_id);
								} else {
									echo get_the_title($target_id);
								} ?>
            </a>
        </h4>
		<p class="ngg-album-gallery-image-counter">
			<?php if (isset($gallery->counter) && $gallery->counter > 0) { ?>
				<strong><?php echo $gallery->counter; ?></strong>&nbsp;<?php _e('Photos', 'nggallery'); ?>
			<?php } else { ?>
				&nbsp;
			<?php } ?>
		</p>
	</div>

 	<?php endforeach; ?>

	<!-- Pagination -->
    <br class="ngg-clear"/>
 	<?php echo $pagination ?>
</div>

<?php endif; ?>
