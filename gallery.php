<?php get_header(); ?>
<?php
  /*
    Template Name: Gallery
  */
?>

<div class="container">
  <?php if ( !is_page(array('Gallery', 'Galleria', 'Галерея'))) { ?>
  <h2 id="gallery-title"><?php echo get_the_title();?></h2>
  <h3 id="back-to-gallery"><a href="<?php
  $current_lang = pll_current_language();
  $gallery_url = get_page_link(get_page_by_title(Gallery));
  $gallery_id = url_to_postid($gallery_url);
  $target_gallery = pll_get_post($gallery_id, $current_lang);
  the_permalink($target_gallery);
  ?>" title="<?php pll_e('Back to gallery'); ?>">« <?php pll_e('Back to gallery'); ?></a></h3>
  <?php } ?>
  <?php
  if ( is_page(array('Gallery', 'Galleria', 'Галерея'))) { ?>
  <div class="iframe-container"><?php
  $content = apply_filters('the_content', $post->post_content);
  echo $content;
  ?></div>
  <?php } else {
  $content = apply_filters('the_content', $post->post_content);
  echo $content;
  }?>
</div>
<?php get_footer(); ?>
