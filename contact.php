<?php
  /*
    Template Name: Contact
  */
  get_header();
 ?>

<div class="row">
  <div class="page-features">
  		<div class="feature-image">
  			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/image3.jpg" alt="" />
  		</div>
  		<div class="feature-telephone">
  			<h4><?php pll_e('Tel'); ?></h4>
  			<p><a href="tel:+358405647204">+358 405647204</a></p>
  		</div>
  		<div class="feature-email">
  			<h4><?php pll_e('Email'); ?></h4>
  			<p><a href="mailto:info@yourdaydesign.com" target="_blank">info@yourdaydesign.com</a></p>
  		</div>
  	</div>

		<div class="page-content">
				<h1><?php pll_e('Lets save the date!'); ?></h1>

        <div class="contact-form">
           <?php
           $page = get_page_by_title('Contact form 1');
           $content = apply_filters('the_content', $post->post_content);
           echo $content;
          ?>
        </div>
     </div>
</div>


 <?php get_footer(); ?>
