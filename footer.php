<div class="clear"></div>
</div>
<footer>

      <div class="row">
        <div class="footer-left"><?php pll_e('Copyright All rights reserved'); ?></div>

        <div class="footer-middle">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/vetka.png" alt="vetochka" width="50" height="70"/></div>
        </div>

        <div class="footer-right"><span><?php pll_e('Tel'); ?></span><a href="tel:+358405647204"> +358405647204 · </a><span><?php pll_e('Email'); ?></span> <a href="mailto:info@yourdaydesign.fi" target="_blank">info@yourdaydesign.com</a></div>

        <div class="credit"><?php pll_e('WEBSITE BY'); echo (' '); ?><a href="http://www.mariaparshina.ml" target="_blank"><?php pll_e('MARIA PARSHINA'); ?></a></div>
      </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
