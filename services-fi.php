<?php
/*
  Template Name: Services FI
*/
  get_header();
?>

<div class="page-features">
  <div class="feature-image">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/macarons.jpg" alt="" />
  </div>
</div>
<div class="page-content">
  <div class="columns">
    <h2>No.1 Hää- ja juhlasuunnittelu</h2>
    <p>Jos odotat tärkeää tapahtumaa, joko häitä, vuosipäivää tai muuta, olet oikeassa paikassa! Your Day Design - on ammattilaisten tiimi, joka pystyy toteuttamaan kaikkea ideoita ja auttaa organisoimaan juhlaa! Koordinointia, valokuvausta ja videota, juhlan tyylin luomista alusta loppuun, neuvot ja apu, musiikki- ja tanssiesityksia, sisustusta, juhlapöydän tai ulkoilmatapahtumien ideointia, nimi- ja kutsukorttien tekemistä, graafista suunnittelua, koordinointia tapahtuman aikana - ei vielä ole täysi luettelo ominaisuuksista, jotka Your Day Design tarjoaa!<br>Ota yhteyttä, ja me luomme Sinulle täydellisen paketin, ja usko pois- me pystymme kaikkeen!</p>

    <h2>No.2 Stailaus​</h2>
    <p>Sehän on niin tärkeätä näyttää juhlapäivänä upealta, haluaisi olla 100 % varma siitä! .Me voimme luvata sinulle, että meidän stylistit tekevät sinusta lumoavan kauniin!</p>
    <ul>
      <li>Style & Make-up Kiiski Valeriia</li>
      <li>Maria Mironova, Styling Academy Koulutus-Center</li>
      <li>Your Look Showroom</li>
      <li>Style Workshop</li>
      <li>Style & Make-up Kiiski Valeriia</li>
      <li>Style & Make-up Kiiski Valeriia</li>
      <li>Julia Sun, Ooppera Helsinki</li>
      <li>Beart Studio, nails and manicure<li>
    </ul>
    <br><br>

    <h2>No.3 Vuokratuotteet</h2>
    <p>Meiltä löytyy todella paljon somistus- ja koristelu tuotteita vuokrattavaksi, ota vain rohkeasti yhteyttä niin säästät omaa aikaa ja rahaa!</p>

    <h2>No.4 Kukat</h2>
    <p>Pienemmistä yksityisjuhlista isompiin häihin meiltä saa ihanimpia kukka-asetelmia ja – kimppuja. On se juhla paljon makeamman näköinen, kun sitä koristellaan luonnolla!<br>
    Meidän ihana floristi: Kukkakauppa Madonna</p>

    <h2>No.5 Graafinen suunnittelu, käsityö</h2>
    <p>Me arvostamme mielettömästi käsityötä ja tästä syystä meiltä saa tilattua kaikenlaisia kutsukortteja, kalligrafiaa, istumajärjestystä, nimikortteja tai vaikka muuten vain kortin muistoksi. Jokainen tilaus tulee olemaan ainutlaatuinen ja käsitelty huolellisesti ja rakkaudella!<br>
    Meidän rakkaat graafiset suunnittelijat: Crocus Paperi, Evaart Design, Daria Vark</p>

    <h2>No.6 Boudoir-, love story kuvaukset</h2>
    <p>Morsiamen aamu - tämä on hetki, jolloin olet täynnä tunteita,jännitystä ja ennakoit jotain erityistä! Odotat kaiken olevan täydellistä! Haluamme kaappata näitä sekunteja muistoksi sinulle! Täydellisesti suunniteltu ja uskomattoman kaunis!</p>

    <h2>No.7 Hääkoreografia</h2>
    <p>Hääpäivänä voi yllättää vieraita häätanssilla! Tanssitunneille tultaessa saatte opittua kauniita liikkeitä ja samalla vietätte laatuaikaa yhdessä!
    <br>Flame Dance Studio</p>

    <h2>No.8 Artistit ja tanssijat</h2>
    <p>Millä tahansa juhlalla voi nauttia live- musiikista uskomattoman lahjakkailta muusikoilta! Esimerkiksi niin hyviä tyyppejä kuten: Anton Morozov, joka on saksofonisti ja Roman Bloshenko, viulisti - ovat molemmat valmistuneet Sibelius Akatemiasta Varastossa meillä on niin sama tanssijoita, laulajia ja bändejä!</p>

    <h2>No.9 Hääkoordinaattori</h2>
    <p>Hääkoordinaattorin palvelut ennen häitä:</p>
    <ul>
      <li>Hääsuunnitelman ennalta kehitys ja aikataulutus</li>
      <li>Juhlapaikan valinta</li>
      <li>Valokuvaajan ja videokuvaajan valinta</li>
      <li>Hääkukkakoristeiden ja koristelutyylin valinta, hääkimpun suunnittelu</li>
      <li>Apu valitessaan morsiamen tyyli, kampaajan ja meikkaajan</li>
      <li>Kutujen suunnittelu, Häätarvikkeiden valmistelu</li>
      <li>Hääohjelman, hääkakun ja leivonnaisten valinta</li>
      <li>Apu istumajärjestyksen laatimiseen<li>
      <li>Häätanssi</li>
      <li>Hotellihuoneen varaus nuoriparille, huoneen koristelu hääyönä</li>
      <li>Häämatkan valinta, häämatkan ohjelma</li>
    </ul><br>
    <p>Hääkoordinaattori palvelut hääpäivänä:</p>
    <ul>
      <li>Valvonta ja koordinointi: kampaaja, meikkitaiteilija, ja muut osallistujat.</li>
      <li>Valvonta juhlapaikan ja auton koristeluista</li>
      <li>Kaikkien osallistujien kokoaminen hääjuhlaan, koordinointi ja vieraiden ottaminen</li>
      <li>Yhteistyötä kaikkien hääosallistujen ja yhteistyökummpaneiden kanssa </li>
      <li>Työn koordinointi ravintolassa (catering, alkoholi, tarjoilijat)</li>
      <li>Lopputili yhteystyökumppaneiden kanssa</li>
      <li>Oleminen tukena ja turvana 24/7</li>
      <li>
    </ul><br>

    <h2>No.10 CKakkumestari ja catering palvelu</h2>
    <p>Jätä herkkujen valmistelun ammattilaisin käsiin! Voit toteuttaa maailman hulluimpia ideoita ,omien maun ja esteettisten mieltymysten mukaan ja koristella juhlaa ihanalla kakulla tai vaikka kokonaisella Candy Barilla!<br>
    Caitlin Linski Cakes&Baking<br>
    ReevesCake</p>

    <h2>No.11 Valokuvaus & Videokuvaus</h2>
    <p>Yksi tärkeimmistä juhlan komponenteista on video- ja valokuvaus. Kaikki mitä jää meille juhlan jälkeen on ainoastaan muistoja, jotka valitettavasti ajan kuluessa muuttuvat mustavalkoiseksi ja himmeiksi. Tämä on juuri syy ikuistaa kaikki tunteet, kaikki kauneus, ilo ja onni- me tarjoamme sinulle parhaita, lahjakkaimmaita ja kokeneempia valokuvaajia ja videokuvaajia!<br>Meidän video- ja valokuvaaja tiimi: Lopyrev Family, Camilla Bloom, Milla Sini, Irina Skoliarova Photography, Alexandr Chernov, Uljana Rättel, Nicole Silver Photography, Nikita Tikka, Nadi Garamova Photography, Onnilab, Sania Svinarenko</p>


    <h2>No.12 Häämätkat</h2>
    <p>Ja nyt, vain te kaksi. Kauan odotettu häämatka! Skazka tour valmistelee kaiken  ja teidän ei tarvitse muuta kuin vain ottaa matkatavarat ja hypätä koneeseen! Yksilöllisen ohjelmat toiveidenne mukaan!</p>
  </div>
</div>
  <div id="service-sets">
    <h1>Hääpaketit</h1>
    <ul>
      <li>
        <h3>1.Day set </h3><br>
        <p>suunnittelutapaaminen 2h,  puhelin- ja sähköpostituki, juhlakoordinointi asiakkaan ohjelman mukaisesti (4h) ​</p><br>
        <p>alk. 350 &euro;</p>
      </li>
      <li>
        <h3>2.Your Day set</h3><br>
        <p>suunnittelutapaaminen 2h​, puhelin- ja sähköpostituki​, uhlakoordinointi asiakkaan ohjelman mukaisesti (4h),  valikoima miedän palveluista</p>
        <p>alk. 1000 &euro;</p>
      </li>
      <li style="border-right:none; border-bottom:none;">
        <h3>3.Your Day Design - full service set</h3><br>
        <p>kokonaisvaltainen hääsuunnittelu paketti</p><br><br>
        <p>alk. 3000 &euro;</p>
      </li>
  </ul>
</div>

<?php get_footer(); ?>
