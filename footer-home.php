<footer>
    <div class="social">
      <ul>
        <li><a href="https://www.facebook.com/yourdaydesign/" target="_blank"><i class="fa fa-facebook-official"></i></a></li>
        <li><a href="https://vk.com/your_day_design" target="_blank"><i class="fa fa-vk"></i></a></li>
        <li><a href="https://www.instagram.com/your_day_design/" target="_blank"><i class="fa fa-instagram"></i></a></li>
      </ul>
    </div>
</footer>


<style>

.social {
  width: 100%;
  margin-top: 100px;
  margin-bottom: 0px;
}
.social ul {
  width: 100%;
  text-align: center;
  margin-bottom: 3%;
}
.social ul li {
  display: inline;
}
.social ul li a {
  padding-left: 1%;
  color: #fff;
}
.social ul li a:hover {
  color: #c47d83;
}

@media only screen and (max-width: 740px) {
  .social {
    margin-top: 400px;
  }
}
@media only screen and (max-width: 570px) {
  .social ul li a {
    font-size: 2em;
    padding-left: 3%;
  }
}

</style>
