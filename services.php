<?php
/*
  Template Name: Services
*/
  get_header();
?>

<div class="page-features">
  <div class="feature-image">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/macarons.jpg" alt="" />
  </div>
</div>
  <div class="page-content">
  	<div class="columns">
  		<h2>No.1 Event & Wedding Design</h2>
      <p>If there is a very important event coming, either is a wedding, anniversary or any other event, you are exactly in the right place! Your Day Design is a team of professionals who are able to realise any of your ideas and help with any kind of organisation. Coordination, photo- and video shooting , advice in event design, assistance in selection of style of the even, choosing musicians and dance accompaniment, interior decor, table setting or outdoor events, generating ideas, writing texts of any complexity, graphic design, maintenance during the event and professional event host - is not a complete list of features that Your Day Design offers!<br>Contact us for a complete service package, or selective quotations, and believe - we can do everything! Our team will help to make your event carefree! We sew happiness from Your Day!</p>

      <h2>No.2 Styling​</h2>
      <p> It’s so important to be sure, that you look amazing on a wedding or other special day. We promise you, that our stylist will make you stunning!</p>
      <ul>
        <li>Style & Make-up Kiiski Valeriia</li>
        <li>Maria Mironova, Styling Academy Koulutus-Center</li>
        <li>Your Look Showroom</li>
        <li>Style Workshop</li>
        <li>Style & Make-up Kiiski Valeriia</li>
        <li>Style & Make-up Kiiski Valeriia</li>
        <li>Julia Sun, Ooppera Helsinki</li>
        <li>Beart Studio, nails and manicure<li>
      </ul>
      <br><br>

      <h2>No.3 Renta​l Decor</h2>
      <p>You can find a lot of decoration for rent, jus contact us shortly and you will save your time and money!</p>

      <h2>No.4 Flower Design</h2>
      <p>We provide incredibly beautiful and each time different floral designs from smallest personal events to bigger weddings. The event look much tastier with a touch of nature!<br>
      Our dear florist: Kukkakauppa Madonna</p>

      <h2>No.5 Graphic Design & Handmade</h2>
      <p>We appreciate handmade work and for that reason you can order any kind of invitations, calligraphy, seating charts, namecards or just a postcard as a souvenir. Every order is unique and made by hands with great affection and love!<br>
      Our dear art and graphic designers: Crocus Paperi, Evaart Design, Daria Vark</p>

      <h2>No.6 Boudoir-, love story photoshoots</h2>
      <p>Bride's morning - this is the moment when you are overwhelmed with feelings of light excitement and anticipation of something special! You wait everything to be perfect! We want to capture such seconds a keepsake for you! Perfectly planned and incredibly tender!</p>

      <h2>No.7 Wedding Choreography​</h2>
      <p>On the wedding day, you can surprise your guests with an amazing dance! While coming to dance classes you will not only learn a few cool "Pa", but will escape from everyday life and spend a wonderful time together!
      <br>Flame Dance Studio</p>

      <h2>No.8 Musicians and dancers</h2>
      <p>On any kind of event is amazing to enjoy live music performed by an incredibly talented musicians! For example, such great guys like: Anton Morozov, a saxophonist and Roman Bloshenko violinist - are both graduates of Helsinki Sibelius Academy. We also have dancers, singers and bands "in stock"!</p>

      <h2>No.9 Confectioner and catering</h2>
      <p>Free yourself from the troublesome feelings on a celebration day, leave cooking gorgeous sweet treats in the hands of true professionals. You will be able to realise any ideas and decorate your day with a wonderful cake holiday or even with whole Candy Bar!<br>
      Caitlin Linski Cakes&Baking<br>
      ReevesCake</p>

      <h2>No.10 Photography & Video</h2>
      <p>One of the most important event details is photo and video. After all only memories are going to be left after celebration, and unfortunately even them will become blurred over time. For capturing of all the emotions, all the beauty, joy and happiness for the long memory - we provide you the best, most talented and most experienced photo and video operators!<br>Our team of photographers and video- operators: Lopyrev Family, Camilla Bloom, Milla Sini, Irina Skoliarova Photography, Alexandr Chernov, Uljana Rättel, Nicole Silver Photography, Nikita Tikka, Nadi Garamova Photography, Onnilab, Sania Svinarenko</p>


      <h2>No.11 Honeymoon</h2>
      <p>And now, just the two of you. The long-awaited honeymoon! Skazka tour will prepare everything for you and you will not need anything but just to collect a bag and catch a flight! Individual programs according to your wishes!</p>
  	</div>
  </div>
    <div id="service-sets">
      <h1>Weddind Service Sets</h1>
      <ul>
        <li>
          <h3>1.Day set </h3><br>
          <p>2h meeting, phone & email support, wedding coordinator accordint to customers wedding program (4h)​</p><br>
          <p>from 350 &euro;</p>
        </li>
        <li>
          <h3>2.Your Day set</h3><br>
          <p>planning meeting 2h, phone & email support,  wedding coordinator accordint to customers wedding program (4h),  services by your choice​</p>
          <p>from 1000 &euro;</p>
        </li>
        <li style="border-right:none; border-bottom:none;">
          <h3>3.Your Day Design - full service set</h3><br>
          <p>Full Wedding Desing set</p><br><br><br><p></p>
          <p>from 3000 &euro;</p>
        </li>
    </ul>
  </div>


<?php get_footer(); ?>

<style>
  	@media only screen and (max-width: 740px) {
      .feature-image img {
        display: none;
      }
      .page-features {
        margin-top: -67px;
      }
    }
</style>
